Feature: Create new registry

  As a user I should be able to create a new registry

  Background:
    Given I am signed up and on the welcome page of registry

@logoutFromDesign
  Scenario: Create new registry
    When I fill al requiered fields for registry
    And I go to the date page
    And I insert the date
    And I select Customize your design 
    Then User creates a new registry and is brougth to designs page

@logoutFromDesign
  Scenario: Create new registry without date
    When I fill al requiered fields for registry
    And I go to the date page
    And I select We haven't decided yet
    And I select Customize your design 
    Then User creates a new registry and is brougth to designs page

@logout
  Scenario: Create new registry with missing field
    When I fill al requiered fields for registry except one
    And I go to the date page
    Then An error from missing field will show
    And Date page button will be disabled

@logoutFromDate
  Scenario: Create new registry with invalid date
    When I fill al requiered fields for registry
    And I go to the date page
    And I insert invalid date
    And I select Customize your design 
    Then A invalid date error will be displayed

@logoutFromDate
  Scenario: Create new registry with a date before 2012
    When I fill al requiered fields for registry
    And I go to the date page
    And I insert a date before 2012
    And I select Customize your design 
    Then A invalid date error will be displayed