Feature: Performing a sign up

  I should be able to sign up.

  Background:
    Given I am on select product page

@logout
  Scenario: Sign up using wedding website
    When I select Wedding Website button 
    And I sign up with correct email and password
    And I select Sign up for free button 
    Then User signs up 


  Scenario Outline: Sign up using wedding website with invalid email
    When I select Wedding Website button 
    And I sign up with missing "<error>" email and password
    And I select Sign up for free button 
    Then An error is displayed showing that email is invalid
    And Sign up for free button will be disabled

    Examples:
    | error   |
    | @       |
    | .com    |


  Scenario: Sign up using wedding website with used email
    When I select Wedding Website button 
    And I sign up with an used email and password
    And I select Sign up for free button 
    Then An error is displayed showing that email is taken
    And Sign up for free button will be disabled


  Scenario: Sign up using wedding website with no email
    When I select Wedding Website button 
    And I sign up with no email and password
    And I select Sign up for free button 
    Then An error is displayed showing that an email is requiered
    And Sign up for free button will be disabled


  Scenario: Sign up using wedding website with a shorter than 8 characters password
    When I select Wedding Website button 
    And I sign up with email and a shorter than 8 caracters password 
    And I select Sign up for free button 
    Then An error is displayed showing that the password is invalid
    And Sign up for free button will be disabled


  Scenario: Sign up using wedding website with no password
    When I select Wedding Website button 
    And I sign up with email and no password
    And I select Sign up for free button 
    Then An error is displayed showing that a password is requiered
    And Sign up for free button will be disabled
