@logoutFromDesign
Feature: Insert user information

  As a user I should be able to insert information

  Background:
    Given I am loged in and didn't complete the registry
    And I am on the Information page

  Scenario: Insert user information 
    When I insert all requiered fields for user information
    And I select Save Changes
    Then An information updated message will be displayed

  Scenario Outline: Insert user information with invalid email
    When I insert all requiered fields for user information
    And I insert email with missing "<error>"
    And I select Save Changes
    Then An error is displayed saying to enter a valid email

    Examples:
    | error   |
    | @       |
    | .com    |

  Scenario: Insert user information with used email
    When I insert all requiered fields for user information
    When I insert used email
    And I select Save Changes
    Then An error message will be displayed

  Scenario Outline: Insert user information with missing field
    When I insert all requiered fields for user information
    And I erase "<field>" field
    And I select Save Changes
    Then An error is displayed showing that a field cannot be empty

    Examples:
    | field   |
    | first   |
    | last    |
    | email   |