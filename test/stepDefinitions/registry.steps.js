import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import SelectProductPage from '../pageobjects/selectProduct.page';
import SignUpPage from '../pageobjects/signUp.page';
import RegistryPage from '../pageobjects/registry.page';
import DesignsPage from '../pageobjects/designs.page';

defineSupportCode(function({ Given, When, Then }) {

  //SCENARIO 1

  Given(/^I am signed up and on the welcome page of registry$/, function() {
    homePage.open()
    homePage.clickSignUpBtn()
    SelectProductPage.clickWeddingWebsiteBtn()
    SignUpPage.fillFields()
    SignUpPage.clickSignUp()
  })


  When(/^I fill al requiered fields for registry$/, function() {
    RegistryPage.fillFieldsRegistry()

  })

  When(/^I go to the date page$/, function() { //A veces se tranca no se xq
    RegistryPage.clickBtn()
  })

  When(/^I insert the date$/, function() { //No uso Date, uso 3 num random con limites
    RegistryPage.fillDate()
  })

  When(/^I select Customize your design$/, function() {
    RegistryPage.clickBtn()

  })

  Then(/^User creates a new registry and is brougth to designs page$/, function() { //Pregunta de waitForVisible() en expect
    expect(DesignsPage.checkTitleDesigns()).to.be.true
  })

  //SCENARIO 2


  When(/^I select We haven't decided yet$/, function() {
    RegistryPage.clickDateCheck()
  })

  Then(/^An error from missing field will show$/, function() {
    expect(RegistryPage.checkFieldError()).to.be.true
    
  })

  Then(/^Date page button will be disabled$/, function() {
    expect(RegistryPage.checkBtnError()).to.be.true
    
  })

  //SCENARIO 3

  When(/^I fill al requiered fields for registry except one$/, function() { //Capaz crear un scenario para cada campo o randomizar el metodo para que agarre cualquier field
    RegistryPage.fillFieldsRegistryExceptOne()
  })

  When(/^I insert invalid date$/, function() {
    RegistryPage.fillOldInvalidDate()
  })

  Then(/^A invalid date error will be displayed$/, function() {
    expect(RegistryPage.checkFieldError()).to.be.true
  })

  Then(/^Customize your design button will be disabled$/, function() {
    expect(RegistryPage.checkBtnErrorDate()).to.be.true
  })

  //SCENARIO 4

  When(/^I insert a date before 2012$/, function() {
    RegistryPage.fillOldInvalidDate()
  })

})

