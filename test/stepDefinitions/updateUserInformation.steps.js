import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import SelectProductPage from '../pageobjects/selectProduct.page';
import SignUpPage from '../pageobjects/signUp.page';
import RegistryPage from '../pageobjects/registry.page';
import InformationPage from '../pageobjects/information.page';
defineSupportCode(function({ Given, When, Then }) {

  //SCENARIO 1

 Given(/^I am loged in and completed the registry$/, function() {
    homePage.open()
    homePage.clickSignUpBtn()
    SelectProductPage.clickWeddingWebsiteBtn()
    SignUpPage.fillFields()
    SignUpPage.clickSignUp()
    RegistryPage.fillFieldsRegistry()
    RegistryPage.clickBtn()
    RegistryPage.fillDate()
    RegistryPage.clickBtn()
  })

  
  
  Given(/^I am on the Information page$/, function() {
    homePage.clickAccountBtn()
  })

  When(/^I insert all requiered fields for user information$/, function() {
    InformationPage.UpdateAllFields()
  })

  When(/^I select Save Changes$/, function() {
    InformationPage.clickBtn()
  })

  Then(/^An information updated message will be displayed$/, function() {
    expect(InformationPage.checkPopUp()).to.be.true
  })

  //SCENARIO 2

  When(/^I insert email with missing "([^"]*)"$/, function(error) {
    InformationPage.fillMailError(error)
  })


  Then(/^An error is displayed saying to enter a valid email$/, function() {
     expect(InformationPage.checkMailError()).to.be.true    
  })


  //SCENARIO 3

  When(/^I insert used email$/, function() {
    InformationPage.fillUsedMail()
  })

  Then(/^An error message will be displayed$/, function() {
     expect(InformationPage.checkMailUsedError()).to.be.true    
  })

  //SCENARIO 4

  When(/^I erase "([^"]*)" field$/, function(field) {
    InformationPage.eraseField(field)
  })

  Then(/^An error is displayed showing that a field cannot be empty$/, function() {
    expect(InformationPage.checkEmptyField()).to.be.true  
  })
  
})

