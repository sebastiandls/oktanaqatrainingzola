import { defineSupportCode } from 'cucumber'
import SelectProductPage from '../pageobjects/selectProduct.page';
import SignUpPage from '../pageobjects/SignUp.page';
import RegistryPage from '../pageobjects/registry.page';

defineSupportCode(function({ When, Then }) {

  //SCENARIO 1

  When(/^I select Wedding Website button$/, function() {
    SelectProductPage.clickWeddingWebsiteBtn()

  })

  When(/^I sign up with correct email and password$/, function() {
    SignUpPage.fillFields()
  })

  When(/^I select Sign up for free button$/, function() {
    SignUpPage.clickSignUp()
  })

  Then(/^User signs up$/, function() {
    expect(RegistryPage.checkTitle()).to.be.true
  })

  //SCENARIO 2

  When(/^I sign up with missing "([^"]*)" email and password$/, function(error) {
    SignUpPage.fillFieldsMailError(error)
    SignUpPage.clickSignUp()
  })

  Then(/^An error is displayed showing that email is invalid$/, function() {
    expect(SignUpPage.checkEmailErrorInvalid()).to.be.true    
  })

  Then(/^Sign up for free button will be disabled$/, function() {
    expect(SignUpPage.checkDisabledBtn()).to.be.true
  })

  //SCENARIO 3

  When(/^I sign up with an used email and password$/, function() {
    SignUpPage.fillFieldsUsed()
    SignUpPage.clickSignUp()
  })

  Then(/^An error is displayed showing that email is taken$/, function() {
    expect(SignUpPage.checkEmailErrorTaken()).to.be.true    
  })

  //SCENARIO 4

  When(/^I sign up with no email and password$/, function() {
    SignUpPage.noEmailAndPassword()
  })

  When(/^An error is displayed showing that an email is requiered$/, function() {
    expect(SignUpPage.checkEmailErrorRequired()).to.be.true    
  })

  //SCENARIO 5

  When(/^I sign up with email and a shorter than 8 caracters password$/, function() {
    SignUpPage.fillFieldsShortPass()
  })

  Then(/^An error is displayed showing that the password is invalid$/, function() {
    expect(SignUpPage.checkPasswordErrorLong()).to.be.true 
  })

  //SCENARIO 6

  When(/^I sign up with email and no password$/, function() {
    SignUpPage.EmailNoPassword()
  })


  Then(/^An error is displayed showing that a password is requiered$/, function() {
    expect(SignUpPage.checkPasswordErrorRequired()).to.be.true 

  })


})

