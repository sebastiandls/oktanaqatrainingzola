import { defineSupportCode } from 'cucumber'
import RegistryPage from '../pageobjects/registry.page';
import HomePage from '../pageobjects/home.page';
import WeddingPlanning from '../pageobjects/weddingPlanning.page';

defineSupportCode(function({
    Before, After
}) {
    
    After({
       tags: '@deleteCookies'
    }, function() {
    	browser.pause(2000)
    	console.log("antes cookies")
      browser.deleteCookies()
      console.log("despues cookies")

    })

    After({
        tags: '@logout'
    }, function() {
        browser.pause(1000)

    	  RegistryPage.clickBackBtn()
    	  HomePage.clickLogout()
    })

    After({
        tags: '@logoutFromDesign'
    }, function() {
        browser.pause(1000)

        HomePage.clickLogout()
        WeddingPlanning.clickLogo()
    })

    After({
        tags: '@logoutFromDate'
    }, function() {
        browser.pause(1000)

        RegistryPage.clickBackBtn()
        RegistryPage.clickBackBtn()
        HomePage.clickLogout()
    })

})