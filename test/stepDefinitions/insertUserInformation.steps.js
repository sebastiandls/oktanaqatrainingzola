import { defineSupportCode } from 'cucumber'
import homePage from '../pageobjects/home.page';
import SelectProductPage from '../pageobjects/selectProduct.page';
import SignUpPage from '../pageobjects/signUp.page';
import RegistryPage from '../pageobjects/registry.page';
import InformationPage from '../pageobjects/information.page';

defineSupportCode(function({ Given }) {

  //SCENARIO 1

  Given(/^I am loged in and didn't complete the registry$/, function() {
    homePage.open()
    homePage.clickSignUpBtn()
    SelectProductPage.clickWeddingWebsiteBtn()
    SignUpPage.fillFields()
    SignUpPage.clickSignUp()
    RegistryPage.clickBackBtn()
  })
  
})

