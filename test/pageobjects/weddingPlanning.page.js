import Page from './page'


class WeddingPlanning extends Page {

  get logo () { return browser.element('div.top-nav__logo a div.nav__logo') }
  

  clickLogo() {
    this.logo.waitForVisible()
    this.logo.click()
  }

}

export default new WeddingPlanning()
