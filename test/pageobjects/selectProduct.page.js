import Page from './page'

class SelectProductPage extends Page {

  get weddingWebsiteBtn ()	{ return browser.element('a#tag-signup-intent-website') }
  get titleSelectProduct () { return browser.element('div.text-h1.intent-title') }


  getTitleSelectProduct() {
  	this.titleSelectProduct.waitForVisible()
  	return this.titleSelectProduct.getText()
  }

  clickWeddingWebsiteBtn() {
    this.weddingWebsiteBtn.waitForVisible()
    this.weddingWebsiteBtn.click()
  }
  
}

export default new SelectProductPage()
