import Page from './page'


class HomePage extends Page {

  get logo ()           { return browser.element('div.top-nav__logo a div.nav__logo') }
  get accountBtn ()     { return browser.element('a.account-link') }
  get signUpBtn ()      { return browser.element('div.top-nav__btn') }
  get registryBtn ()    { return browser.element('li.dropdown a.taxonomy-root')[0] }
  get logout ()         { return browser.element('div.account-dropdown__container ul li:last-child') }
  get account ()        { return browser.element('li.top-nav__link.hidden-sm.account-link__container a.account-link') }



  open () {
    super.open('/')
    this.logo.waitForVisible()
  }

  clickAccountBtn() {
    this.accountBtn.waitForVisible()
    this.accountBtn.click()
  }

  clickSignUpBtn() {
    this.signUpBtn.waitForVisible()
    this.signUpBtn.click()
  }

  clickLogout() {
    browser.moveToObject(this.account.selector);
    this.logout.waitForVisible()
    this.logout.click()
  }

}

export default new HomePage()
