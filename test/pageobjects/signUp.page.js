import Page from './page'
import constants from '../constants.js'

class SignUpPage extends Page {

    get email()             { return browser.element('input#signup-email')}
    get password()          { return browser.element('input#signup-password')}
    get signUpBtn()         {return browser.element('button.btn.btn-primary.btn-wide')}
    get signUpBtnDisabled() {return browser.element('button.btn.btn-primary.btn-wide.disabled')}
    get title()             {return browser.element('div.text-h1.intent-title')}
    get emailError()        { return browser.element('input#signup-email + span.help-block') }
    get passwordError()     {  return browser.element('input#signup-password + span.help-block') }


    clickSignUp() {
        this.signUpBtn.waitForVisible()
        browser.pause(500)
        this.signUpBtn.click()
    }

    fillFields() {
        let random1 = this.random()
        let random2 = this.random()
        let pass = this.randomPass()
        let mail = random1 + "@" + random2 + ".com"

        this.email.waitForVisible()
        this.email.setValue(mail)

        this.email.waitForVisible()
        this.password.setValue(pass)
    }

    fillFieldsMailError(error) {
        let random1 = this.random()
        let random2 = this.random()
        let pass = this.randomPass()
        let mail

        if (error === "@") {
            mail = random1 + random2 + ".com"
        } else if (error === ".com") {
            mail = random1 + "@" + random2
        }

        this.email.waitForVisible()
        this.email.setValue(mail)

        this.password.waitForVisible()
        this.password.setValue(pass)
    }

    fillFieldsUsed() {
        this.email.waitForVisible()
        this.email.setValue(constants.usedEmail)

        this.password.waitForVisible()
        this.password.setValue(constants.usedPassword)
    }

    fillFieldsShortPass() {
        let random1 = this.random()
        let random2 = this.random()
        let pass = this.randomShort()
        let mail = random1 + "@" + random2 + ".com"

        this.email.waitForVisible()
        this.email.setValue(mail)

        this.password.waitForVisible()
        this.password.setValue(pass)

        this.email.click()
    }

    random() {
        return Math.random().toString(36).substring(4);
    }

    randomShort() {
        return Math.random().toString(36).substring(8);
    }

    isEmailErrorExisting() {
        return this.emailError.waitForVisible()
    }

    isPasswordErrorExisting() {
        return this.passwordError.waitForVisible()
    }

    checkEmailErrorInvalid() {
        return this.isEmailErrorExisting() && this.emailError.getText() === constants.emailErrorInvalid
    }

    checkEmailErrorTaken() {
        return  this.isEmailErrorExisting() && this.emailError.getText() === constants.emailErrorTaken
    }

    checkEmailErrorRequired() {
        return this.isEmailErrorExisting() && this.emailError.getText() === constants.ErrorRequired
    }

    checkPasswordErrorLong() {
        return this.isPasswordErrorExisting() && this.passwordError.getText() === constants.PasswordErrorLong
    }

    checkPasswordErrorRequired() {
        return this.isPasswordErrorExisting() && this.passwordError.getText() === constants.ErrorRequired
    }

    checkDisabledBtn() {
        return this.signUpBtnDisabled.isVisible()
    }

    noEmailAndPassword() {
        this.email.waitForVisible()
        this.email.click()
        this.password.waitForVisible()
        this.password.setValue(this.randomShort())
    }

    EmailNoPassword() {
        let random1 = this.random()
        let random2 = this.random()
        let mail = random1 + "@" + random2 + ".com"

        this.email.waitForVisible()
        this.email.setValue(mail)

        this.password.waitForVisible()
        this.password.click()

        this.email.click()
    }

    randomPass(){ // A veces el random() tiraba strings menores que 8 caracteres y hacia trancar cuando ingresabas la password
        let random
        while (true) {
            random  =  Math.random().toString(36).substring(4);
            console.log('randomPass: ', random)
            if (random < 8) {
                continue
            } else {
                return random
            }
        }
    }

}

export default new SignUpPage()