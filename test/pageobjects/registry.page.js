import Page from './page'
import constants from '../constants.js'

class RegistryPage extends Page {

  get registryTitle ()      { return browser.element('div#step-1 h3.onboard-step-header.hidden-xs') }
  get primaryFirstName ()   { return browser.element('input#primaryFirstName') }
  get primaryLastName ()    { return browser.element('input#primaryLastName')  }
  get partnerFirstName ()   { return browser.element('input#partnerFirstName')  }
  get partnerLastName ()    { return browser.element('input#partnerLastName')  }
  get primaryRoleBride ()   { return browser.element('input#primaryRoleBride + span')  }
  get primaryRoleGroom ()   { return browser.element('input#primaryRoleGroom + span') }
  get partnerRoleBride ()   { return browser.element('input#partnerRoleBride + span')  }
  get partnerRoleGroom ()   { return browser.element('input#partnerRoleGroom + span') }
  get btn ()   			        { return browser.element('button.btn.btn-primary.btn-wide') }
  get btnError ()           { return browser.element('button.btn.btn-primary.btn-wide.disabled') }
  get btnBack ()            { return browser.element('button.btn.btn-secondary') }
  get dateField ()   		    { return browser.element('input[name="wedding_date"]') }
  get dateCheck ()   	    	{ return browser.element('span.input-override') }
  get img ()                { return browser.element('img[alt="Zola Cake"]') }
  get fieldError ()         { return browser.element('span.help-block') }


  clickBtn() {
  	this.btn.waitForVisible()
  	this.btn.click()
  }

  clickBackBtn() {
    this.btnBack.waitForVisible()
    browser.pause(300)
    this.btnBack.click()
  }

  clickDateCheck() {
    this.dateCheck.waitForVisible()
    this.dateCheck.click()
  }

  checkTitle() {
    this.registryTitle.waitForVisible()
    if (this.registryTitle.getText() === constants.registryTitle){
      return true
    }else {
      return false
    }
  }

  fillFieldsRegistry() {
    this.primaryFirstName.waitForVisible()
    this.primaryFirstName.setValue(this.random())

    this.primaryLastName.waitForVisible()
    this.primaryLastName.setValue(this.random())

    this.primaryRoleGroom.waitForVisible()
    this.primaryRoleGroom.click()

    this.partnerFirstName.waitForVisible()
    this.partnerFirstName.setValue(this.random())

    this.partnerLastName.waitForVisible()
    this.partnerLastName.setValue(this.random())

    this.partnerRoleBride.waitForVisible()
    this.partnerRoleBride.click()
  }


  fillFieldsRegistryExceptOne() {
    this.primaryFirstName.waitForVisible()
    this.primaryFirstName.setValue(this.random())

    this.primaryLastName.waitForVisible()
    this.primaryLastName.setValue(this.random())

    this.primaryRoleGroom.waitForVisible()
    this.primaryRoleGroom.click()

    this.partnerFirstName.waitForVisible()
    this.partnerFirstName.setValue(this.random())

    this.partnerRoleBride.waitForVisible()
    this.partnerRoleBride.click()

    this.partnerLastName.click()
    this.partnerFirstName.click()

  }

  fillDate() {
    let day = this.randomNum(1, 28)
    let month = this.randomNum(1, 12)
    let year = this.randomNum(2019, 2028)

    if (day < 10) { day = "0" + day }
    if (month < 10) { month = "0" + month }

    let date = month + "/" + day + "/" + year

    this.dateField.waitForVisible()
    this.dateField.setValue(date)

    this.img.click()

    this.btn.waitForVisible()
    this.btn.click()
  }

 randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }

  fillOldInvalidDate() {
    let date = this.randomNum(1, 12) + "/" + this.randomNum(1, 28) + "/" + this.randomNum(1900, 2017)

    this.dateField.waitForVisible()
    this.dateField.setValue(date)

    this.img.click()

    this.btn.waitForVisible()
    this.btn.click()
  }

  fillInvalidDate() {
    let date = this.randomNum(12, 30) + "/" + this.randomNum(28, 40) + "/" + this.randomNum(1900, 2040)

    this.dateField.waitForVisible()
    this.dateField.setValue(date)

    this.img.click()

    this.btn.waitForVisible()
    this.btn.click()
  }

  clickBtn() {
    this.btn.waitForVisible()
    this.btn.click()
  }

  checkFieldError() {
    return this.fieldError.isVisible()
  }

  checkBtnError() {
    return this.btnError.isVisible()
  }

  randomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  random() {
    return Math.random().toString(36).substring(4);
  }

}

export default new RegistryPage()
