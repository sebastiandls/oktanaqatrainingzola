import Page from './page'
import constants from '../constants.js'
import { World } from '../World';

World.erasedField

class InformationPage extends Page {

  get informationTitle ()   { return browser.element('h2.zo-h2') }
  get firstName ()     		{ return browser.element('input[name="firstName"]') }
  get lastName ()    		{ return browser.element('input[name="lastName"]') }
  get email ()     			{ return browser.element('input[name="email"]') }
  get saveBtn ()     		{ return browser.element('input.btn.btn-primary.btn-md') }
  get popUp ()     			{ return browser.element('div.humane.humane-zola-success div') }
  get emailError()        { return browser.element('input[name="email"] + span.help-block') }
  get emailUsedError()        { return browser.element('div.humane.humane-zola-error div') }
  get emptyFieldError() { return browser.element('span.help-block') }


  clickAccountBtn() {
    this.accountBtn.waitForVisible()
    this.accountBtn.click()
  }

  clickBtn() {
  	this.saveBtn.waitForVisible()
  	this.saveBtn.click()
  }

  checkPopUp() {
  	browser.pause(1000)
  	return this.popUp.waitForVisible()
  }

  eraseField(field){
    if (field === "first") {
      this.firstName.waitForVisible()
      this.firstName.setValue("")
    }else if (field === "last") {
      this.lastName.waitForVisible()
      this.lastName.setValue("")
    }else if (field === "email") {
      this.email.waitForVisible()
      this.email.setValue("")
    }

    World.erasedField = field
    this.informationTitle.click()
  }

  UpdateAllFields() {
  	this.firstName.waitForVisible()
  	this.firstName.setValue(this.randomString())

  	this.lastName.waitForVisible()
  	this.lastName.setValue(this.randomString())

  	this.email.waitForVisible()

  	let random1 = this.randomString()
    let random2 = this.randomString()
    let mail = random1 + "@" + random2 + ".com"

    this.email.setValue(mail)
  }

  fillMailError(error) {
  	let random1 = this.randomString()
    let random2 = this.randomString()
    let mail

    if (error === "@") {
        mail = random1 + random2 + ".com"
    } else if (error === ".com") {
        mail = random1 + "@" + random2
    }

    console.log(mail)
    this.email.setValue(mail)
  }

  fillUsedMail() {
    this.email.waitForVisible()
    this.email.setValue(constants.usedEmail)
  }

  checkEmptyFieldText() { 
    if (World.erasedField === "first" || World.erasedField === "last") {
      if (this.emptyFieldError.getText() === constants.emptyNameError) {
        return true
      }
    } else if (World.erasedField === "email") {
      if (this.emptyFieldError.getText() === constants.emptyEmailError) {
        return true
      }
    } else {
      return false
    }
  }

  checkEmptyField() {
    return this.emptyFieldError.waitForVisible() && this.checkEmptyFieldText()
  }

  checkMailUsedError(){
    return this.emailUsedError.waitForVisible()
  }

  isEmailErrorExisting() {
    return this.emailError.waitForVisible() 
   }

  checkMailError() {
    return this.isEmailErrorExisting() && this.emailError.getText() === constants.emailInformationErrorInvalid
  }

  randomString () {
    return Math.random().toString(36).substring(7);
  }

}

export default new InformationPage()
