import Page from './page'

class DesignsPage extends Page {

  get titleDesigns () { return browser.element('div.col-xs-12.col-sm-9 h3') }
  get logout ()       { return browser.element('div.account-dropdown__container ul li:last-child') }
  get account ()      { return browser.element('li.top-nav__link.hidden-sm.account-link__container a.account-link') }

  checkTitleDesigns() {
	  this.titleDesigns.waitForVisible()
    return this.titleDesigns.getText() === "Choose Your Wedding Website Design"
  }

  clickLogout() {
    browser.moveToObject(this.account.selector);
    this.logout.waitForVisible()
    this.logout.click()
  }
  
}

export default new DesignsPage()
