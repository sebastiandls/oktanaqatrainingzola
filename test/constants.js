module.exports = {
registryTitle: "Welcome, this is the wedding of",
emailErrorInvalid: "Invalid email address",
emailErrorTaken: "This email address is taken.",
ErrorRequired: "Required",
PasswordErrorLong: "Password must be at least 8 characters long.",
emailInformationErrorInvalid: "Please enter a valid email address.",
usedEmail: "sebastiandls@oktana.com",
usedPassword: "password",
emptyNameError: "This field is required.",
emptyEmailError: "Please enter a valid email address.",

}